package com.atlassian.connect.starterapp.service;

import java.util.List;

import com.atlassian.connect.starterapp.domain.Comment;

public interface CommentService {

    List<Comment> getComments();

    List<Comment> addComment(Comment comment);
}
