package com.atlassian.connect.starterapp.dao;

import com.atlassian.connect.starterapp.domain.Comment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {
    Page<Comment> findAll(Pageable pageable);

    Page<Comment> findByAuthor(String author, Pageable pageable);
}
